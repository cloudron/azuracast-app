This app is pre-setup with an admin account. The initial credentials are:

**Email**: admin<br/>
**Password**: changeme123<br/>

Please change the admin password and email immediately on the admin profile page.

**Listeners statistics**

To get `Listeners statistics` collected and shown correctly you should set `local_infile=1` MySQL system variable that permits local data loading by clients. You need to ssh to Cloudron host and make MySql config file changes:

1. get into MySql docker container

```
$ docker exec -ti mysql /bin/bash
```

2. Edit /run/mysql/my.cnf

```
[mysqld]
local-infile = 1
```

3. Restart MySQL service to apply changes

```
$ supervisorctl restart mysql && exit
```

4. To check if the value is properly set you should run the following command on the Cloudron host:

```
$ docker exec -ti mysql mysql -p$(docker inspect mysql | grep ROOT_PASSWORD | sed -e 's/.*".*=//g' -e 's/",//g') -e "SHOW GLOBAL VARIABLES like '%local_infile%'"
```

*Note:* These settings will be lost after the app restart.

