#!/usr/bin/env node

/* jshint esversion: 8 */
/* global describe */
/* global before */
/* global after */
/* global it */
/* global xit */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    readlinePromises = require('readline/promises'),
    expect = require('expect.js'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

const rl = readlinePromises.createInterface({ input: process.stdin, output: process.stdout });

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = 'test';
    const TEST_TIMEOUT = 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const ADMIN_USERNAME = "admin";
    const ADMIN_EMAIL = "admin@cloudron.local";
    const ADMIN_PASSWORD = "changeme123";

    const STATION_NAME = 'Cloudron';

    let browser, app;
    let say_cmd;

    before(function (done) {
        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
        done();
    });

    after(function () {
        browser.quit();
    });

    function sleep(millis) {
        return new Promise(resolve => setTimeout(resolve, millis));
    }

    async function get_say_cmd() {
        if (typeof(say_cmd) == 'undefined' || say_cmd == null)
            say_cmd = String(await execSync('uname -s')).trim() == "Darwin" ? "say" : "spd-say";
        return say_cmd;
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function setup() {
        await browser.get(`https://${app.fqdn}/`);

        await waitForElement(By.id('hdr_new_station'));
        await browser.findElement(By.id('edit_form_name')).sendKeys(STATION_NAME);
        let button = await browser.findElement(By.xpath('//button[text()="Create and Continue"]'));
        await browser.executeScript('arguments[0].scrollIntoView(true)', button);
        await sleep(3000);
        button.click();

//        await waitForElement(By.id('hdr_system_settings'));
        await waitForElement(By.id('edit_form_base_url'));

        await sleep(2000);
        await browser.findElement(By.id('edit_form_base_url')).sendKeys(`https://${app.fqdn}`);
//        await browser.findElement(By.xpath('//input[@id="edit_form_base_url"]')).sendKeys(`https://${app.fqdn}`);
        button = await browser.findElement(By.xpath('//button[text()="Save and Continue"]'));
        await browser.executeScript('arguments[0].scrollIntoView(true)', button);
        await sleep(3000);
        button.click();

        await waitForElement(By.xpath(`//div[contains(@class, "h5") and text()="${STATION_NAME}"]`));
    }

    async function login(username, password) {
        await browser.get(`https://${app.fqdn}/login`);

        await waitForElement(By.id('username'));

        await browser.findElement(By.id('username')).sendKeys(username);
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.findElement(By.xpath('//button[text()="Sign In"]')).submit();

        await sleep(2000);

        await waitForElement(By.xpath('//h3[@class="card-subtitle" and text()="' + username + '"] | //div[@class="me-2" and text()="' + username + '"]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}/dashboard`);

        await browser.sleep(2000);

        await waitForElement(By.xpath('//button[contains(@aria-label, "Toggle Menu")]'));
        await browser.findElement(By.xpath('//button[contains(@aria-label, "Toggle Menu")]')).click();

        await waitForElement(By.xpath('//a[@href="/logout"]'));
        await browser.findElement(By.xpath('//a[@href="/logout"]')).click();

        await waitForElement(By.id('username'));
    }

    async function openStationProfileUrl() {
        await browser.get('https://' + app.fqdn + '/dashboard');
        await waitForElement(By.xpath(`//div[contains(@class, "h5") and text()="${STATION_NAME}"]`));

        let button = browser.findElement(By.xpath(`//div[contains(@class, "h5") and text()="${STATION_NAME}"]/parent::td/following-sibling::td/a[text()="Manage"]`));
        await browser.executeScript('arguments[0].scrollIntoView(true)', button);
        await sleep(3000);

        await button.click();

        await waitForElement(By.xpath('//a[@href="#sidebar-submenu-media"]'));
        

        await sleep(2000);
        return await browser.getCurrentUrl();
    }

    async function uploadMedia() {
        let stationUrl = await openStationProfileUrl();

        await waitForElement(By.xpath('//a[@href="#sidebar-submenu-media"]'));
        await browser.findElement(By.xpath('//a[@href="#sidebar-submenu-media"]')).click();
        await sleep(2000);

        await browser.findElement(By.xpath('//div[@id="sidebar-submenu-media"]//a[text()="Music Files"]')).click();

        await waitForElement(By.id('hdr_music_files'));

        // upload music file
        await browser.findElement(By.xpath('//input[@type="file"]')).sendKeys(path.resolve('./podcast_episode.mp3'));

        // wait when the file uploaded
        await sleep(5000);

        await browser.findElement(By.xpath('//span[text()="Select All Rows"]/preceding-sibling::input[@class="form-check-input" and @role="switch"]')).click();
        await sleep(2000);

        // add media to default playlist
        await browser.findElement(By.xpath('//button[contains(., "Playlists")]')).click();
        await sleep(2000);
        await browser.findElement(By.xpath('//label[contains(., "default")]')).click();
        await sleep(2000);
        await browser.findElement(By.xpath('//button[text()="Save"]')).click();
        await sleep(2000);
        await waitForElement(By.xpath(`//td/a[text()="default"]`));
    }

    async function restartStation() {
        let stationUrl = await openStationProfileUrl();

        // open restart station page
        await browser.get(stationUrl + 'restart');

        await waitForElement(By.xpath('//button[text()="Reload Configuration"]'));
        await browser.findElement(By.xpath('//button[text()="Reload Configuration"]')).click();
        await sleep(2000);

        await waitForElement(By.xpath('//button[contains(@class, "swal2-confirm")]'));
        await browser.findElement(By.xpath('//button[contains(@class, "swal2-confirm")]')).click();
        await sleep(5000);
//        await waitForElement(By.xpath('//div[text()="Station reloaded."]'));
    }

    async function setupStation() {

        await uploadMedia();
        await restartStation();
    }

    async function checkStation() {
        await openStationProfileUrl();

        await waitForElement(By.xpath('//button[@aria-label="Play"]'));
        await browser.findElement(By.xpath('//button[@aria-label="Play"]')).click();

        await browser.sleep(2000);
        let say_cmd = await get_say_cmd();
        execSync(`${say_cmd} "Is music playing?"`);
        await rl.question('Is music playing? ');
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    it('can install app', async function () {
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        await sleep(10000);
    });
    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));
    it('can setup', setup);
    it('can logout', logout);
    it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));

    it('can setup station', setupStation);
    it('check station', checkStation);
    it('can logout', logout);

    it('can restart app', async function () {
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(5000);
    });

    it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));
    it('check station', checkStation);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));
    it('check station', checkStation);
    it('can logout', logout);

    it('move to different location', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));
    it('check station', checkStation);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

/*
    // test update
    it('can install app', function () { execSync(`cloudron install --appstore-id com.azuracast.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));
    it('can setup', setup);
    it('can logout', logout);
    it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));

    it('can setup station', setupStation);
    it('check station', checkStation);
    it('can logout', logout);

    it('can update', async function () {
        execSync(`cloudron update --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
    });

    it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));
    it('check station', checkStation);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
*/
});
