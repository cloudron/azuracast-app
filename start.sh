#!/bin/bash

set -eu

echo "==> App starting"
echo -ne "\033[42m Wait when all services are up and running \033[0m"
i=0; while [ "${i}" -lt 12 ]; do echo -ne "\033[42m. \033[0m"; sleep 1; i=$((i+1)); done; echo " "

wait_for_app() {
    set -eu

    while ! curl --fail -s http://localhost:80 > /dev/null; do
        echo "Waiting for app to come up..."
        sleep 1
    done
}

create_admin() {
    wait_for_app

    adminUsersCount=$(mysql -u"${CLOUDRON_MYSQL_USERNAME}" -p"${CLOUDRON_MYSQL_PASSWORD}" -h "${CLOUDRON_MYSQL_HOST}" -P "${CLOUDRON_MYSQL_PORT}" ${CLOUDRON_MYSQL_DATABASE} -NB -e "SELECT count(*) FROM user_has_role WHERE role_id=1" 2>/dev/null)
    if [[ $adminUsersCount -eq 0 ]]; then
        echo "==> Creating admin user: admin@cloudron.local / changeme123"
        csrf=$(curl -c /tmp/cookies.txt -s ${CLOUDRON_APP_ORIGIN}/setup/register | grep '"csrf":"[^"]*"' | sed -E 's/.*"csrf":"([^"]+)".*/\1/')
        curl -s -X POST -b /tmp/cookies.txt -H 'content-type: application/x-www-form-urlencoded' -d "csrf=${csrf}&username=admin%40cloudron.local&password=changeme123" "${CLOUDRON_APP_ORIGIN}/setup/register"
    fi
}

echo "==> Creating directories"
mkdir -p /app/data/servers/shoutcast2 \
        /app/data/servers/stereo_tool \
        /app/data/storage/shoutcast2 \
        /app/data/storage/stereo_tool \
        /app/data/storage/uploads \
        /app/data/storage/geoip \
        /app/data/storage/acme \
        /app/data/storage/sftpgo \
        /app/data/stations \
        /app/data/servers \
        /app/data/backups \
        /app/data/uploads \
        /app/data/geoip \
        /app/data/meilisearch/persist \
        /run/nginx/nginx_client \
        /run/nginx/nginx_fastcgi \
        /run/nginx/nginx_cache \
        /run/php \
        /run/azuracast/www_tmp

touch /run/nginx/.tmpreaper

touch /run/nginx/nginx_client/.tmpreaper
touch /run/nginx/nginx_fastcgi/.tmpreaper
touch /run/nginx/nginx_cache/.tmpreaper
touch /run/php8.3-fpm.log

chown azuracast:azuracast -R /run/nginx /run/php8.3-fpm.log

# https://github.com/AzuraCast/AzuraCast/blob/0.19.7/util/docker/web/setup/nginx.sh
chmod -R 777 /run/nginx/nginx_*

# SFTPGO
if [[ ! -d /app/data/sftpgo ]]; then
    cp -R /var/azuracast/sftpgo.orig /app/data/sftpgo
    cp /app/pkg/sftpgo.json /app/data/sftpgo/sftpgo.json
fi

if [[ ! -f /app/data/storage/sftpgo/id_rsa ]]; then
    ssh-keygen -t rsa -b 4096 -f /app/data/storage/sftpgo/id_rsa -q -N ""
fi

if [[ ! -f /app/data/storage/sftpgo/id_ecdsa ]]; then
    ssh-keygen -t ecdsa -b 521 -f /app/data/storage/sftpgo/id_ecdsa -q -N ""
fi

if [[ ! -f /app/data/storage/sftpgo/id_ed25519 ]]; then
    ssh-keygen -t ed25519 -f /app/data/storage/sftpgo/id_ed25519 -q -N ""
fi
echo "SFTPGO_SFTPD__BINDINGS__0__PORT=${AZURACAST_SFTP_PORT:-2022}" > /app/data/sftpgo/env.d/sftpd.env

# Create sftpgo temp dir
mkdir -p /tmp/sftpgo_temp
touch /tmp/sftpgo_temp/.tmpreaper
chmod -R 777 /tmp/sftpgo_temp

# DBIP
if [[ ! -d /app/data/dbip ]]; then
    mkdir -p /app/data/dbip

    YEAR=$(date +'%Y')
    MONTH=$(date +'%m')

    echo "==> Downloading DB-IP data"
    wget -qO - "https://download.db-ip.com/free/dbip-city-lite-${YEAR}-${MONTH}.mmdb.gz" | gunzip > /app/data/dbip/dbip-city-lite.mmdb
fi

if [[ ! -d /app/data/web ]]; then
    echo "==> Creating web files"
    cp -r /app/pkg/icecast_custom_files/web /app/data
fi

if [[ ! -f /app/data/storage/acme/ssl.crt ]]; then
    ln -s /etc/certs/tls_cert.pem /app/data/storage/acme/ssl.crt
    ln -s /etc/certs/tls_key.pem /app/data/storage/acme/ssl.key
fi


echo "==> Preparing Environment"
AUTO_ASSIGN_PORT_MIN=${STATIONS_PORTS}
AUTO_ASSIGN_PORT_MAX=$((${STATIONS_PORTS}+${STATIONS_PORTS_COUNT}))
AZURACAST_STATION_PORTS=$(seq -s "," ${AUTO_ASSIGN_PORT_MIN} ${AUTO_ASSIGN_PORT_MAX})

cat > /run/azuracast/azuracast.env <<EOF
APPLICATION_ENV=true
AZURACAST_STATION_PORTS=${AZURACAST_STATION_PORTS}
AUTO_ASSIGN_PORT_MIN=${AUTO_ASSIGN_PORT_MIN}
AUTO_ASSIGN_PORT_MAX=${AUTO_ASSIGN_PORT_MAX}
IS_DOCKER=true
MYSQL_HOST=${CLOUDRON_MYSQL_HOST}
MYSQL_PORT=${CLOUDRON_MYSQL_PORT}
MYSQL_USER=${CLOUDRON_MYSQL_USERNAME}
MYSQL_PASSWORD=${CLOUDRON_MYSQL_PASSWORD}
MYSQL_DATABASE=${CLOUDRON_MYSQL_DATABASE}
ENABLE_REDIS=true
REDIS_HOST=${CLOUDRON_REDIS_HOST}
REDIS_PORT=${CLOUDRON_REDIS_PORT}
REDIS_DB=1
NGINX_RADIO_PORTS=default
NGINX_WEBDJ_PORTS=default
PREFER_RELEASE_BUILDS=false
COMPOSER_PLUGIN_MODE=false
ADDITIONAL_MEDIA_SYNC_WORKER_COUNT=0
PROFILING_EXTENSION_ENABLED=0
PROFILING_EXTENSION_ALWAYS_ON=0
PROFILING_EXTENSION_HTTP_KEY=dev
PROFILING_EXTENSION_HTTP_IP_WHITELIST=*
ENABLE_WEB_UPDATER=false
ENABLE_MEILISEARCH=false
MEILISEARCH_MASTER_KEY=""
EOF

if [[ ! -f /app/data/azuracast.env ]]; then
    cat > /app/data/azuracast.env << EOT

# This allows you to log debug-level errors temporarily (for problem-solving) or reduce
# the volume of logs that are produced by your installation, without needing to modify
# whether your installation is a production or development instance.
# Valid options: debug, info, notice, warning, error, critical, alert, emergency
LOG_LEVEL=debug

# This allows you to debug Slim Application Errors you may encounter
# By default, this is disabled to prevent users from seeing privileged information
# Please report any Slim Application Error logs to the development team on GitHub
# Valid options: true, false
SHOW_DETAILED_ERRORS=true
EOT
fi
cat /app/data/azuracast.env >> /run/azuracast/azuracast.env

if [[ ! -f /run/azuracast/env.sh ]]; then
    echo "export $(grep -v '^#' /run/azuracast/azuracast.env | xargs)" > /run/azuracast/env.sh
fi
source /run/azuracast/env.sh

# centrifugo
if [[ ! -d "/app/data/centrifugo" ]]; then
    mkdir -p /app/data/centrifugo
    # v0.19.6+
    sed -e 's/{{ if isTrue .Env.ENABLE_REDIS }}//g' \
        -e 's/{{ .Env.REDIS_HOST }}:{{ default .Env.REDIS_PORT "6379" }}/'${CLOUDRON_REDIS_HOST}':'${CLOUDRON_REDIS_PORT}'/g' \
        -e 's/{{ end }}//g' \
        /app/pkg/centrifugo-config.toml.tmpl > /app/data/centrifugo/config.toml

fi

# PHP settings
if [[ ! -f /app/data/php.ini ]]; then
    cat > /app/data/php.ini <<EOT
; Add custom PHP configuration in this file
; Settings here are merged with the package's built-in php.ini

display_errors=1
error_reporting="E_ALL"

[php]
post_max_size=50M
upload_max_filesize=50M
memory_limit=256M
max_execution_time=30

[opcache]
opcache.enable=1
opcache.enable_cli=1

opcache.revalidate_freq=60

; Enable FFI (for StereoTool inspection)
ffi.enable="true"
EOT

fi

echo "==> Updating nginx config"
cat /app/pkg/azuracast-nginx.conf > /run/azuracast/azuracast-nginx.conf

echo "==> DB migration"
source /run/azuracast/env.sh && php bin/console migrations:migrate -n -v

( create_admin ) &

echo "==> Changing ownership"
chown -R azuracast:azuracast /app/data /run/azuracast

echo "==> Starting Azuracast"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Azuracast
