#
# Icecast-KH with AzuraCast customizations build step
#
FROM ghcr.io/azuracast/icecast-kh-ac:2024-02-13 AS icecast

#
# Built-in docs build step
#
FROM ghcr.io/azuracast/azuracast.com:builtin AS docs


FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg/azuracast /var/azuracast

ARG DEBIAN_FRONTEND=noninteractive

ARG VERSION=0.20.1
ARG ARCHITECTURE=amd64
ARG LIQUIDSOAP_VERSION=2.2.5


# add a non-previleged user that apps can use
# by default, account is created as inactive which prevents login via openssh
RUN adduser --uid 1010 --disabled-login --gecos 'Azuracast' azuracast && \
    passwd -d azuracast && \
    usermod -a -G media azuracast

RUN curl -L https://github.com/AzuraCast/AzuraCast/archive/refs/tags/${VERSION}.tar.gz | tar -xz --strip-components=1 -C /app/pkg/azuracast

# v0.19.6+
RUN wget -O /app/pkg/centrifugo-config.toml.tmpl https://github.com/AzuraCast/AzuraCast/raw/${VERSION}/util/docker/web/centrifugo/config.toml.tmpl

RUN add-apt-repository -y ppa:chris-needham/ppa
RUN add-apt-repository -y ppa:ondrej/php
RUN add-apt-repository -y ppa:ondrej/nginx-mainline

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    tini tzdata \
    vorbis-tools flac \
    libxslt1.1 \
    tmpreaper \
    zstd \
    netbase \
    libid3tag0 \
    libboost-program-options1.74.0 \
    libboost-filesystem1.74.0 \
    libboost-regex1.74.0

# https://github.com/AzuraCast/AzuraCast/blob/e9255d2cbedf9ceffd65c28bce41ab5a38a541fb/util/docker/web/setup/audiowaveform.sh#L14
RUN wget -O /tmp/audiowaveform.deb "https://github.com/bbc/audiowaveform/releases/download/1.10.1/audiowaveform_1.10.1-1-12_amd64.deb" && \
    dpkg -i /tmp/audiowaveform.deb && \
    apt-get install -y -f --no-install-recommends && \
    rm -f /tmp/audiowaveform.deb

# install php 8.3
RUN apt-get install -y --no-install-recommends \
    php8.3 php8.3-{bcmath,bz2,cgi,cli,common,curl,dba,dev,enchant,fpm,gd,gmp,imap,interbase,intl,ldap,mbstring,mysql,odbc,opcache,pgsql,phpdbg,pspell,readline,snmp,soap,sqlite3,sybase,tidy,xml,xmlrpc,xsl,zip,mysqlnd,maxminddb} libapache2-mod-php8.3

# remove php 8.1
RUN apt-get purge -y php8.1 php8.1-{bcmath,bz2,cgi,cli,common,curl,dba,dev,enchant,fpm,gd,gmp,imap,interbase,intl,ldap,mbstring,mysql,odbc,opcache,pgsql,phpdbg,pspell,readline,snmp,soap,sqlite3,sybase,tidy,xml,xmlrpc,xsl,zip} libapache2-mod-php8.1

# Add PHP extension installer tool
#COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/bin/
COPY install-php-extensions /usr/bin/

# https://github.com/AzuraCast/AzuraCast/blob/0.19.5/util/docker/web/setup/php.sh#L5
RUN install-php-extensions @composer gmp maxminddb ffi sockets
#RUN composer install gmp maxminddb ffi sockets


# install Go lang v1.19.9
ARG GO_VERSION=1.19.9
RUN curl -L https://go.dev/dl/go${GO_VERSION}.linux-amd64.tar.gz | tar -xz -f - -C /usr/local
ENV PATH=$PATH:/usr/local/go/bin

# maybe dockerize needs to be removed 
RUN go install github.com/jwilder/dockerize@v0.6.1 && \
    mv ~/go/bin/dockerize $(dirname $(which go))

RUN go install github.com/centrifugal/centrifugo/v4@v4.1.5 && \
    mv ~/go/bin/centrifugo $(dirname $(which go))

# Add built-in docs
COPY --from=docs --chown=azuracast:azuracast /dist /var/azuracast/docs

WORKDIR /app/code

RUN mkdir -p /app/code/www && \
    ln -sf /app/data/storage /app/code/storage && \
    ln -sf /app/data/storage /var/azuracast/storage && \
    ln -sf /app/data/stations /app/code/stations && \
    ln -sf /app/data/servers /app/code/servers && \
    ln -sf /app/data/backups /app/code/backups && \
    ln -sf /app/data/backups /var/azuracast/backups && \
    ln -sf /run/azuracast/www_tmp /app/code/www_tmp && \
    ln -sf /app/data/uploads /app/code/uploads && \
    ln -sf /app/data/geoip /app/code/geoip && \
    ln -sf /app/data/dbip /app/code/dbip && \
    ln -sf /app/data/centrifugo /app/code/centrifugo


# Build Icecast from source
# https://github.com/AzuraCast/AzuraCast/blob/0.18.5/util/docker/stations/setup/icecast.sh
RUN apt-get update && \
    apt-get install -q -y --no-install-recommends \
        build-essential libxml2 libxslt1-dev libvorbis-dev libssl-dev libcurl4-openssl-dev openssl

COPY --from=icecast /usr/local/bin/icecast /usr/local/bin/icecast
COPY --from=icecast /usr/local/share/icecast /usr/local/share/icecast

RUN mkdir -p /tmp/icecast_custom_files

# Remove build tools
RUN apt-get remove --purge -y build-essential libxslt1-dev libvorbis-dev libssl-dev libcurl4-openssl-dev

# Copy AzuraCast Icecast customizations
RUN curl -L https://github.com/AzuraCast/icecast-kh-custom-files/archive/refs/tags/2023-04-23.tar.gz | tar -xz --strip-components=1 -C /tmp/icecast_custom_files

RUN mv /usr/local/share/icecast/web /usr/local/share/icecast/web.orig && \
    mv /tmp/icecast_custom_files /app/pkg/icecast_custom_files && \
    ln -sf /app/data/web /usr/local/share/icecast/web && \
    rm -rf /tmp/icecast_custom_files
#     rm -rf /tmp/icecast_build

WORKDIR /app/code/www

# Liquidsoap
#
# https://github.com/AzuraCast/AzuraCast/blob/e9255d2cbedf9ceffd65c28bce41ab5a38a541fb/util/docker/stations/setup/liquidsoap.sh
#
# Packages required by Liquidsoap
RUN apt-get install -y --no-install-recommends \
    libao4 libfaad2 libfdk-aac2 libgd3 liblo7 libmad0 libmagic1 libportaudio2 \
    libsdl2-image-2.0-0 libsdl2-ttf-2.0-0 libsoundtouch1 libxpm4 \
    libasound2 libavcodec58 libavdevice58 libavfilter7 libavformat58 libavutil56 \
    libpulse0 libsamplerate0 libswresample3 libswscale5 libtag1v5 \
    libsrt1.4-openssl bubblewrap ffmpeg liblilv-0-0 \
    libjemalloc2

# Audio Post-processing
RUN apt-get install -y --no-install-recommends ladspa-sdk

RUN wget -O /tmp/liquidsoap.deb https://github.com/savonet/liquidsoap/releases/download/v2.2.4/liquidsoap_2.2.4-ubuntu-jammy-2_amd64.deb
RUN dpkg -i /tmp/liquidsoap.deb && \
    apt-get install -y -f --no-install-recommends && \
    rm -f /tmp/liquidsoap.deb && \
    ln -s /usr/bin/liquidsoap /usr/local/bin/liquidsoap

# master_me install
# https://github.com/AzuraCast/AzuraCast/blob/e9255d2cbedf9ceffd65c28bce41ab5a38a541fb/util/docker/stations/setup/master_me.sh
RUN mkdir -p /tmp/master_me /usr/lib/ladspa /usr/lib/lv2
RUN wget -O /tmp/master_me/master_me.tar.xz "https://github.com/trummerschlunk/master_me/releases/download/1.2.0/master_me-1.2.0-linux-x86_64.tar.xz"
RUN tar -xvf /tmp/master_me/master_me.tar.xz --strip-components=1 -C /tmp/master_me

RUN mv /tmp/master_me/master_me-easy-presets.lv2 /usr/lib/lv2 && \
    mv /tmp/master_me/master_me.lv2 /usr/lib/lv2 && \
    mv /tmp/master_me/master_me-ladspa.so /usr/lib/ladspa/master_me.so && \
    rm -rf /tmp/master_me


RUN cp /app/pkg/azuracast/composer.json /app/code/www && \
    cp /app/pkg/azuracast/composer.lock /app/code/www && \
    composer install \
        --no-dev \
        --no-ansi \
        --no-autoloader \
        --no-interaction && \
    composer clear-cache

RUN cp -R /app/pkg/azuracast/. ./

RUN composer dump-autoload --optimize --classmap-authoritative

WORKDIR /app/code/www/frontend
RUN npm ci --include=dev && \
    npm run build && \
    npm cache clean --force

# Sensible default environment variables.
ENV TZ="UTC" \
    LANG="en_US.UTF-8" \
    PATH="${PATH}:/app/code/servers/shoutcast2" \
    DOCKER_IS_STANDALONE="true" \
    APPLICATION_ENV="production"
RUN touch /app/code/.docker

# clean up
RUN apt-get -y autoremove && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /tmp/tmp*

# configure nginx & php-fpm
RUN rm /etc/nginx/sites-enabled/* && \
    mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.orig && \
    mv /etc/nginx/proxy_params /etc/nginx/proxy_params.orig && \
    ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log && \
    ln -sf /run/php8.3-fpm.log /var/log/php8.3-fpm.log && \
    ln -sf /run/azuracast/azuracast-nginx.conf /etc/nginx/sites-enabled/azuracast.conf

COPY nginx/proxy_params.conf /etc/nginx/proxy_params
COPY nginx/nginx.conf /etc/nginx/nginx.conf
RUN sed -i 's#application/vnd.apple.mpegurl#application/x-mpegurl#' /etc/nginx/mime.types

RUN wget -O /app/pkg/php-fpm.conf.tmpl https://github.com/AzuraCast/AzuraCast/raw/${VERSION}/util/docker/web/php/www.conf.tmpl

RUN sed -z 's/{{if eq .Env.PROFILING_EXTENSION_ENABLED "1"}}.*{{end}}//g' \
        /app/pkg/php-fpm.conf.tmpl \
    | sed -e 's/pm.max_children = .*$/pm.max_children = 20/g' \
        > /etc/php/8.3/fpm/pool.d/www.conf

COPY nginx/azuracast-nginx.conf /app/pkg/azuracast-nginx.conf
COPY nginx/readonlyrootfs.conf /etc/nginx/conf.d/readonlyrootfs.conf

RUN ln -s /app/data/php.ini /etc/php/8.3/cli/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.3/fpm/conf.d/99-cloudron.ini

WORKDIR /app/code/www
RUN chown -R azuracast:azuracast /app/code/www


RUN pip3 install --no-cache-dir \
        git+https://github.com/coderanger/supervisor-stdout

RUN wget -O /etc/supervisor/supervisord.conf https://github.com/AzuraCast/AzuraCast/raw/${VERSION}/util/docker/supervisor/supervisor/supervisord.conf && \
    sed -e 's/logfile=\/var\/azuracast\/www_tmp\/supervisord.log/logfile=\/run\/azuracast\/supervisord.log/' \
        -i /etc/supervisor/supervisord.conf && \
    echo -e "[include]\nfiles = /etc/supervisor/conf.d/*.conf /app/code/stations/*/config/supervisord.conf" >> /etc/supervisor/supervisord.conf


COPY supervisor/* /etc/supervisor/conf.d/

# cron
# COPY --from=go-dependencies /go/bin/supercronic /usr/local/bin/supercronic
# https://github.com/aptible/supercronic/releases/tag/v0.2.29
ENV SUPERCRONIC_URL=https://github.com/aptible/supercronic/releases/download/v0.2.29/supercronic-linux-amd64 \
    SUPERCRONIC=supercronic-linux-amd64 \
    SUPERCRONIC_SHA1SUM=cd48d45c4b10f3f0bfdd3a57d054cd05ac96812b

RUN curl -fsSLO "$SUPERCRONIC_URL" \
 && echo "${SUPERCRONIC_SHA1SUM}  ${SUPERCRONIC}" | sha1sum -c - \
 && chmod +x "$SUPERCRONIC" \
 && mv "$SUPERCRONIC" "/usr/local/bin/${SUPERCRONIC}" \
 && ln -s "/usr/local/bin/${SUPERCRONIC}" /usr/local/bin/supercronic

#RUN wget -O /etc/cron.d/azuracast https://github.com/AzuraCast/AzuraCast/raw/${VERSION}/util/docker/web/cron/azuracast
RUN echo "0 * * * * * gosu azuracast bash -c \"source /run/azuracast/env.sh && php /app/code/www/bin/console azuracast:sync:run\"" > /etc/cron.d/azuracast && \
    echo "0 30 */6 * * * * tmpreaper 12h --verbose --protect '.tmpreaper' --protect 'proxies' --protect 'service_*.log' /run/nginx /app/code/stations/*/temp /run/azuracast/www_tmp" >> /etc/cron.d/azuracast

# SFTPGO
# https://github.com/AzuraCast/AzuraCast/blob/0.19.7/util/docker/web/setup/sftpgo.sh
RUN wget -O /tmp/sftpgo.deb "https://github.com/drakkan/sftpgo/releases/download/v2.5.6/sftpgo_2.5.6-1_${ARCHITECTURE}.deb"
RUN dpkg -i /tmp/sftpgo.deb && \
    apt-get install -y -f --no-install-recommends && \
    rm -f /tmp/sftpgo.deb

RUN mkdir -p /var/azuracast/sftpgo/persist \
            /var/azuracast/sftpgo/backups \
            /var/azuracast/sftpgo/env.d

RUN wget -O /app/pkg/sftpgo.json https://github.com/AzuraCast/AzuraCast/raw/${VERSION}/util/docker/web/sftpgo/sftpgo.json

RUN touch /var/azuracast/sftpgo/sftpgo.db

RUN mv /var/azuracast/sftpgo /var/azuracast/sftpgo.orig && \
    ln -sf /app/data/sftpgo /var/azuracast/sftpgo

RUN ln -sf /run/azuracast/azuracast.env /app/code/www/azuracast.env

COPY start.sh /app/pkg/

RUN chmod +x /app/pkg/start.sh

CMD [ "/app/pkg/start.sh" ]
