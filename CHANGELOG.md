[0.1.0]
 * Initial version for AzuraCast

[0.2.0]
* Update AzuraCast to 0.20.1
* [Full changelog](https://github.com/AzuraCast/AzuraCast/releases/tag/0.20.1)

[0.3.0]
* Add missing package metadata
